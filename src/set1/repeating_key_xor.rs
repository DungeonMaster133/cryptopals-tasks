use crate::set1::fixed_xor::fixed_xor;
use std::cmp::min;
use hex::encode;


pub fn repeating_key_xor(s: String, key: String) -> String {
    
    let key_length = key.len();
    let s_length = s.len();
    let mut result = "".to_string();
    for start in (0..s_length).step_by(key_length) {
        let end = min(start + key_length, s_length);
        let block_xor = encode(
            fixed_xor(s[start..end].as_bytes().to_vec(),
            &key[0..end-start].as_bytes().to_vec()));
        result += &block_xor;
    }
    
    result
}
