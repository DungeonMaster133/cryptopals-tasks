pub fn fixed_xor(mut s1: Vec<u8>, s2: &Vec<u8>) -> Vec<u8> {
    
    s1.iter_mut()
        .zip(s2.iter())
        .for_each(|(x1, x2)| *x1 ^= *x2);
    
    s1
}
