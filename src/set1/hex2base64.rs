use base64::encode;
use hex::decode;


pub fn hex2base64(hex: String) -> String {

    let bytes = decode(hex).unwrap();

    encode(&bytes)
}
