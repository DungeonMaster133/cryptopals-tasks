pub fn detect_aes_ecb(data: &Vec<u8>) -> bool {
    let block_length = 16;
    for index in 0..(data.len() - block_length*2) {
        let first = &data[index..index + block_length];
        for second_index in (index + block_length)..(data.len() - block_length) {
            let second = &data[second_index..second_index + block_length];
            if first == second {
                return true
            }
        }
    }

    false
}