use hex::decode;
use crate::set1::single_byte_xor::decode_single_byte_xor;


pub fn detect_single_char_xor_from_list(list: Vec<String>) -> String {

    let mut result = "".to_string();
    let mut max_sum = 0;

    for string in list.iter() {
        let bytes = decode(string).unwrap();
        let (current, sum, _key) = decode_single_byte_xor(bytes);
        if sum > max_sum {
            max_sum = sum;
            result = current;
        }
    }
    
    result
}
