use openssl::symm::{Cipher, Mode, Crypter};


pub fn aes_ecb_decrypt(data: Vec<u8>, key: &Vec<u8>) -> Vec<u8> {
    
    let mut cr = Crypter::new(
        Cipher::aes_128_ecb(),
        Mode::Decrypt,
        &key,
        None).unwrap();
    cr.pad(false);
    let mut unciphered_data = vec![0; data.len() + Cipher::aes_128_ecb().block_size()];
    let count = cr.update(&data, &mut unciphered_data).unwrap();
    let rest = cr.finalize(&mut unciphered_data[count..]).unwrap();
    unciphered_data.truncate(count + rest);
    
    unciphered_data
}

pub fn aes_ecb_encrypt(data: Vec<u8>, key: &Vec<u8>) -> Vec<u8> {
    
    let mut cr = Crypter::new(
        Cipher::aes_128_ecb(),
        Mode::Encrypt,
        &key,
        None).unwrap();
    cr.pad(false);
    let mut ciphered_data = vec![0; data.len() + Cipher::aes_128_ecb().block_size()];
    let count = cr.update(&data, &mut ciphered_data).unwrap();
    let rest = cr.finalize(&mut ciphered_data[count..]).unwrap();
    ciphered_data.truncate(count + rest);

    ciphered_data
}
