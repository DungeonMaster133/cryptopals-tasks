use hamming::weight;
use itertools::Itertools;
use std::usize::MAX;
use crate::set1::fixed_xor::fixed_xor;
use crate::set1::single_byte_xor::decode_single_byte_xor;


pub fn hamming_distance(bytes1: Vec<u8>, bytes2: Vec<u8>) -> usize {
    
    let xor = fixed_xor(bytes1, &bytes2);

    weight(&xor) as usize
}

pub fn break_repeating_key_xor(bytes: Vec<u8>) -> Vec<u8> {
    
    let mut min_distance = MAX;
    let mut key_size = 0;
    let mut key = Vec::<u8>::new();
    
    for key_length in 1..40 {
        let mut distance = 0;
        let it = (0..4).combinations(2);
        for comb in it {
            distance += hamming_distance(
                bytes[key_length*comb[0]..key_length*(comb[0]+1)].to_vec(),
                bytes[key_length*comb[1]..key_length*(comb[1]+1)].to_vec()) / key_length;
        }
        if distance < min_distance {
            min_distance = distance;
            key_size = key_length;
        }
    }
    
    for key_index in 0..key_size {
        let mut index_block = Vec::<u8>::new();
        for index in (key_index..bytes.len()).step_by(key_size) {
            index_block.push(bytes[index]);
        }
        let (_res, _sum, key_byte) = decode_single_byte_xor(index_block);
        key.push(key_byte);
    }

    key
}
