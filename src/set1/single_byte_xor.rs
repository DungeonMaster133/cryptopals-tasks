use std::collections::HashMap;


pub fn decode_single_byte_xor(bytes: Vec<u8>) -> (String, usize, u8) {

    let mut max_sum: usize = 0;
    let mut res_key: u8 = 0;
    let mut result: String = "".to_string();
    let mut freq = HashMap::new();
    freq.insert(" ", 15000);
    freq.insert("e", 12000);
    freq.insert("t", 9000);
    freq.insert("a", 8000);
    freq.insert("i", 8000);
    freq.insert("n", 8000);
    freq.insert("o", 8000);
    freq.insert("s", 8000);
    freq.insert("h", 6400);
    freq.insert("r", 6200);
    freq.insert("d", 4400);
    freq.insert("l", 4000);
    freq.insert("u", 3400);
    freq.insert("c", 3000);
    freq.insert("m", 3000);
    freq.insert("f", 2500);
    freq.insert("w", 2000);
    freq.insert("y", 2000);
    freq.insert("g", 1700);
    freq.insert("p", 1700);
    freq.insert("b", 1600);
    freq.insert("v", 1200);
    freq.insert("k", 800);
    freq.insert("q", 500);
    freq.insert("j", 400);
    freq.insert("x", 400);
    freq.insert("z", 200);
    for key in 0..=255 {
        let mut sum = 0;
        let mut decrypted = Vec::<u8>::with_capacity(bytes.len());
        for byte in bytes.iter() {
            let chr = byte ^ key;
            decrypted.push(chr);
        }
        let s = String::from_utf8(decrypted);
        let s = match s {
            Ok(s) => s,
            Err(_err) => "".to_string()
        };
        for (c, c_freq) in &freq {
            let c_count = s.matches(c).count();
            sum += c_count * c_freq;
        }
        if sum > max_sum {
            max_sum = sum;
            result = s;
            res_key = key;
        }
    }

    (result, max_sum, res_key)
}