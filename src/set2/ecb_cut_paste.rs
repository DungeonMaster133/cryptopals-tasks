use crate::set1::aes_ecb::{aes_ecb_encrypt, aes_ecb_decrypt};
use crate::set2::pkcs7_padding::pkcs7_padding;
use rand::Rng;


pub struct Profile {
    pub email: String,
    pub uid: u64,
    pub role: String
}

pub fn profile_to_struct(profile: String) -> Profile {

    let split = profile.split("&");
    let pieces = split.collect::<Vec<&str>>();

    let mut profile_struct = Profile {
        email: "".to_string(),
        uid: 0,
        role: "".to_string()
    };

    for piece in pieces.iter() {
        let key_value_split = piece.split("=");
        let key_value = key_value_split.collect::<Vec<&str>>();
        let key = key_value[0];
        let value = key_value[1];
        if key == "email" {
            profile_struct.email = value.to_string();
        }
        else if key == "uid" {
            let uid = value.to_string().parse::<u64>().unwrap();  
            profile_struct.uid = uid;
        }
        else if key == "role" {
            profile_struct.role = value.to_string();
        }
    }

    profile_struct
}

pub fn profile_for(email: String) -> String {
    
    let mut rng = rand::thread_rng();
    let mut string = email;
    string = string.replace("&", "");
    string = string.replace("=", "");
    let uid = rng.gen_range(10..100).to_string();

    let mut profile = "email=".to_string();
    profile.push_str(&string);
    profile.push_str(&"&uid=");
    profile.push_str(&uid);
    profile.push_str(&"&role=user");

    profile
}

pub fn profile_encrypt(data: Vec<u8>, key: &Vec<u8>) -> Vec<u8> {

    let mut with_padding = data.clone();
    if data.len() % 16 != 0 {
        let full_block_count = ((data.len() / 16) as f32).floor() as usize;
        with_padding = data[..(full_block_count * 16)].to_vec();
        let mut padding = pkcs7_padding(
            data[(full_block_count * 16)..data.len()].to_vec(), 16);
        with_padding.append(&mut padding);
    }

    aes_ecb_encrypt(with_padding, &key)
}

pub fn profile_decrypt(data: Vec<u8>, key: &Vec<u8>) -> Profile {
    
    let decrypted_data = aes_ecb_decrypt(data, &key);
    let mut dec = vec![];
    for i in &decrypted_data {
        if *i != 4 {
            dec.push(*i);
        }
    }
    
    profile_to_struct(String::from_utf8(dec).unwrap())
}

pub fn create_cookie_with_admin_role(key: &Vec<u8>) -> Vec<u8> {
    let email = "12345678901234@foo".to_string(); // 2 blocks with uid=??
    let encoded_profile = profile_for(email);
    let enc_profile = profile_encrypt(encoded_profile.as_bytes().to_vec(), &key);
    let enc_role_admin = profile_encrypt("role=admin".as_bytes().to_vec(), &key); // 1 block
    
    let mut cookie_with_admin = enc_profile[..32].to_vec();
    cookie_with_admin.extend(&enc_role_admin);

    cookie_with_admin
}
