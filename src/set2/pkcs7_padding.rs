pub fn pkcs7_padding(block: Vec<u8>, block_length: usize) -> Vec<u8> {
    if block.len() != block_length {
        let mut new_block: Vec<u8> = block.clone();
        new_block[..block.len()].clone_from_slice(&block);
        // for _ in block.len()..block_length {
            // new_block.push(4);
        // }
        new_block.resize(block_length, 4);
        return new_block
    }
    
    block
}

pub fn pkcs7_padding_full(data: &mut Vec<u8>, block_length: usize) {
    
    if data.len() % block_length != 0 {
        let blocks_count = ((data.len() / block_length) as f32).floor() as usize;
        let padding = pkcs7_padding(
            data[(blocks_count * block_length)..data.len()].to_vec(), block_length);
        data.resize((blocks_count + 1) * block_length, 0);
        data[blocks_count * block_length..].copy_from_slice(&padding);
    }
}
