use crate::set1::aes_ecb::{aes_ecb_encrypt, aes_ecb_decrypt};
use crate::set1::fixed_xor::fixed_xor;


pub fn aes_cbc_decrypt(data: Vec<u8>, key: &Vec<u8>, iv: &Vec<u8>) -> Vec<u8> {
    let block_length = key.len();
    let mut decrypted_data = vec![];
    let mut iv_tmp = iv.to_owned();
    for block_start in (0..(data.len())).step_by(block_length) {
        let decrypted_block = aes_ecb_decrypt(data[block_start..(block_start + block_length)].to_vec(), key);
        let xored = fixed_xor(decrypted_block, &iv_tmp);
        decrypted_data.extend(&xored);
        iv_tmp = data[block_start..(block_start + block_length)].to_vec().clone();
    }

    decrypted_data
}

pub fn aes_cbc_encrypt(data: Vec<u8>, key: &Vec<u8>, iv: &Vec<u8>) -> Vec<u8> {
    let block_length = key.len();
    let mut encrypted_data = vec![];
    let mut iv_tmp = iv.to_owned();
    for block_start in (0..(data.len())).step_by(block_length) {
        let xored = fixed_xor(data[block_start..(block_start + block_length)].to_vec(), &iv_tmp);
        let encrypted_block = aes_ecb_encrypt(xored, key);
        encrypted_data.extend(&encrypted_block);
        iv_tmp = encrypted_block.clone();
    }

    encrypted_data
}
