use crate::set2::aes_cbc::aes_cbc_encrypt;
use crate::set1::detect_aes_ecb::detect_aes_ecb;
use crate::set1::aes_ecb::aes_ecb_encrypt;
use crate::set2::pkcs7_padding::pkcs7_padding;
use rand::Rng;


pub fn add_random_bytes(data: Vec<u8>) -> Vec<u8> {

    let mut result = vec![];
    let mut rng = rand::thread_rng();
    
    let left_side_len = rng.gen_range(5..11);
    let mut left_side = vec![0; left_side_len];
    for el in left_side.iter_mut() {
        *el = rng.gen::<u8>();
    }
    let right_side_len = rng.gen_range(5..11);
    let mut right_side = vec![0; right_side_len];
    for el in right_side.iter_mut() {
        *el = rng.gen::<u8>();
    }
    
    result.extend(left_side);
    result.extend(data);
    result.extend(right_side);
    
    result
}

pub fn encryption_oracle(data: Vec<u8>) -> Vec<u8> {
    
    let mut rng = rand::thread_rng();
    let data_with_random = add_random_bytes(data);
    let mut full_blocks: Vec<u8>;
    if data_with_random.len() % 16 != 0 {
        let full_block_count = ((data_with_random.len() / 16) as f32).floor() as usize;
        full_blocks = data_with_random[..(full_block_count * 16)].to_vec();
        let mut padding = pkcs7_padding(
            data_with_random[(full_block_count * 16)..data_with_random.len()].to_vec(), 16);
        full_blocks.append(&mut padding);
    }
    else {
        full_blocks = data_with_random;
    }
    let mut key = vec![0; 16];
    for el in key.iter_mut() {
        *el = rng.gen::<u8>();
    }

    let encrypted_data: Vec<u8>;
    if rng.gen_range(0..2) == 0 {
        encrypted_data = aes_ecb_encrypt(full_blocks, &key);
    }
    else {
        let mut iv = vec![0; 16];
        for el in iv.iter_mut() {
            *el = rng.gen::<u8>();
        }
        encrypted_data = aes_cbc_encrypt(full_blocks, &key, &iv);
    }

    encrypted_data
}

pub fn detect_aes_mode(encrypted_data: Vec<u8>) -> String {
    if detect_aes_ecb(&encrypted_data) {
        return "ECB".to_string()
    }

    "CBC".to_string()
}
