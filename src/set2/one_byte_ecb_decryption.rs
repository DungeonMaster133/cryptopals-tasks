use crate::set2::pkcs7_padding::pkcs7_padding;
use crate::set1::aes_ecb::aes_ecb_encrypt;


pub fn encryption_oracle_with_added_data(data: &Vec<u8>, key: &Vec<u8>) -> Vec<u8> {
    
    let mut new_data = data.to_owned();
    let base64_added_data = "Um9sbGluJyBpbiBteSA1LjAKV2l0aCBteSByYWctdG9wIGRvd24gc28gbXkgaGFpciBjYW4gYmxvdwpUaGUgZ2lybGllcyBvbiBzdGFuZGJ5IHdhdmluZyBqdXN0IHRvIHNheSBoaQpEaWQgeW91IHN0b3A/IE5vLCBJIGp1c3QgZHJvdmUgYnkK".as_bytes();
    let mut decoded_added_data = base64::decode(base64_added_data).unwrap();
    new_data.append(&mut decoded_added_data);

    let mut full_blocks: Vec<u8>;
    if new_data.len() % 16 != 0 {
        let full_block_count = ((new_data.len() / 16) as f32).floor() as usize;
        full_blocks = new_data[..(full_block_count * 16)].to_vec();
        let mut padding = pkcs7_padding(
            new_data[(full_block_count * 16)..new_data.len()].to_vec(), 16);
        full_blocks.append(&mut padding);
    }
    else {
        full_blocks = new_data.to_vec();
    }
    
    let encrypted_data: Vec<u8>;
    encrypted_data = aes_ecb_encrypt(full_blocks, &key);

    encrypted_data
}

pub fn one_byte_ecb_decryption(key: &Vec<u8>) -> Vec<u8> {

    let enc_length = encryption_oracle_with_added_data(&vec![], &key).len();
    let mut dec_added_data = vec![];
    for byte in 0..enc_length {
        let input_length = 15 - (byte % 16);
        let input_data = vec![4; input_length];
        let encrypted_data = encryption_oracle_with_added_data(&input_data, &key);
        for chr in 0..=255 {
            let mut current_input_data = input_data.clone();
            current_input_data.extend(&dec_added_data);
            current_input_data.push(chr);
            let enc = encryption_oracle_with_added_data(
                &current_input_data, &key);
            if enc[..input_length + byte] == encrypted_data[..input_length + byte] {
                dec_added_data.push(chr);
                break
            }
        }
    }

    dec_added_data
}
