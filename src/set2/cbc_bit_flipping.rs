use crate::set2::aes_cbc::{aes_cbc_decrypt, aes_cbc_encrypt};
use crate::set2::pkcs7_padding::pkcs7_padding_full;


pub fn oracle(userdata: &Vec<u8>, key: &Vec<u8>, iv: &Vec<u8>) -> Vec<u8> {
    
    let prefix = "comment1=cooking%20MCs;userdata=".as_bytes().to_vec();
    let postfix = ";comment2=%20like%20a%20pound%20of%20bacon".as_bytes().to_vec();

    let mut full_message: Vec<u8> = vec![];
    full_message.extend(&prefix);
    full_message.extend(String::from_utf8(
        userdata.to_vec()).unwrap().replace(
            ";", "';'").replace("=", "'='").as_bytes().to_vec());
    full_message.extend(&postfix);
    pkcs7_padding_full(&mut full_message, 16);

    aes_cbc_encrypt(full_message, key, iv)
}

pub fn oracle_is_admin(data: &Vec<u8>, key: &Vec<u8>, iv: &Vec<u8>) -> bool{

    let decrypted_text = String::from_utf8(
        aes_cbc_decrypt(data.to_vec(), key, iv)[32..].to_vec()).unwrap();

    if decrypted_text.contains(";admin=true;") {
        return true
    }

    false
}

pub fn cookie_with_admin_role_attack(encrypted_data: &mut Vec<u8>, userdata: &Vec<u8>, start_index: usize) {

    let target_string = ";admin=true".as_bytes().to_vec();

    for index in 0..target_string.len() {
        encrypted_data[start_index + index] ^= target_string[index] ^ userdata[index];
    }
}
