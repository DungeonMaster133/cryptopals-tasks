use crate::set2::pkcs7_padding::pkcs7_padding;
use crate::set1::aes_ecb::aes_ecb_encrypt;


pub fn encryption_oracle_complete(data: &Vec<u8>, rand_prefix: &Vec<u8>, key: &Vec<u8>) -> Vec<u8> {

    let block_length = 16;
    let mut new_data = rand_prefix.to_owned();
    new_data.extend(data);    
    let base64_added_data = "Um9sbGluJyBpbiBteSA1LjAKV2l0aCBteSByYWctdG9wIGRvd24gc28gbXkgaGFpciBjYW4gYmxvdwpUaGUgZ2lybGllcyBvbiBzdGFuZGJ5IHdhdmluZyBqdXN0IHRvIHNheSBoaQpEaWQgeW91IHN0b3A/IE5vLCBJIGp1c3QgZHJvdmUgYnkK".as_bytes();
    let decoded_added_data = base64::decode(base64_added_data).unwrap();
    new_data.extend(&decoded_added_data);

    let mut full_blocks: Vec<u8>;
    if new_data.len() % block_length != 0 {
        let full_block_count = ((new_data.len() / block_length) as f32).floor() as usize;
        full_blocks = new_data[..(full_block_count * block_length)].to_vec();
        let mut padding = pkcs7_padding(
            new_data[(full_block_count * block_length)..new_data.len()].to_vec(), block_length);
        full_blocks.append(&mut padding);
    }
    else {
        full_blocks = new_data.to_vec();
    }
    
    let encrypted_data: Vec<u8>;
    encrypted_data = aes_ecb_encrypt(full_blocks, &key);

    encrypted_data
}

pub fn one_byte_ecb_decryption_with_prefix(key: &Vec<u8>, rand_prefix: &Vec<u8>) -> Vec<u8> {

    let block_length: usize = 16;
    
    let mut found = false;
    let mut data_start_index: i32 = 0;
    let mut input_length = block_length * 3;
    loop {
        let check_prefix_input = vec![5; input_length];
        let check_prefix_enc = encryption_oracle_complete(
            &check_prefix_input, &rand_prefix, &key);
        for index in 0..(check_prefix_enc.len() - block_length * 2) {
            let first = &check_prefix_enc[index..index + block_length];
            for second_index in (index + block_length)..(check_prefix_enc.len() - block_length) {
                let second = &check_prefix_enc[second_index..second_index + block_length];
                if first == second {
                    found = true;
                    data_start_index = index as i32;
                    break;
                }
            }
            if found {
                break;
            }
        }
        if !found {
            break
        }
        found = false;
        input_length -= 1;
    }
    // можно узнать точную длину, если убавлять по одному байту из входных данных
    // когда он не сможет найти повторные блоки, то длина будет = исходная длина(кратная блокам) - длина блока + кол-во убранных байт
    data_start_index -= block_length as i32 - ((block_length * 3) - input_length) as i32 + 1;
    
    let basis_length = block_length - data_start_index as usize % block_length;
    let enc_length = encryption_oracle_complete(
        &vec![], rand_prefix, key).len() - data_start_index as usize;
    let mut dec_added_data = vec![];
    for byte in 0..enc_length {
        let input_length = block_length - 1 - (byte % block_length) + basis_length;
        let input_data = vec![4; input_length];
        let encrypted_data = encryption_oracle_complete(
            &input_data, rand_prefix, key)[data_start_index as usize..].to_vec();
        for chr in 0..=255 {
            let mut current_input_data = input_data.clone();
            current_input_data.extend(&dec_added_data);
            current_input_data.push(chr);
            let enc = encryption_oracle_complete(
                &current_input_data, rand_prefix, key)[data_start_index as usize..].to_vec();
            if encrypted_data[..input_length + byte] == enc[..input_length + byte] {
                dec_added_data.push(chr);
                break
            }
        }
    }

    del_padding(&mut dec_added_data);
    dec_added_data
}

pub fn del_padding(data: &mut Vec<u8>) {
    data.retain(|x| *x != 4);
}
