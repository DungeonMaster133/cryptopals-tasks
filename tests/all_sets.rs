use cryptopals_tasks::set1::{hex2base64, fixed_xor, single_byte_xor,
                             detect_single_char_xor, repeating_key_xor,
                             break_repeating_key_xor, aes_ecb, detect_aes_ecb};
use cryptopals_tasks::set2::{pkcs7_padding, aes_cbc, one_byte_ecb_decryption,
                             ecb_cut_paste, one_byte_ecb_decryption_hard,
                             cbc_bit_flipping};
use hex::{encode, decode};
use rand::Rng;
use base64;
use std::fs::File;
use std::io::{self, BufRead};
use std::path::Path;


#[cfg(test)]
mod tests {
    use super::*;
    
    #[test]
    fn ch1() {
        assert_eq!(hex2base64::hex2base64(
            "49276d206b696c6c696e6720796f757220627261696e206c696b65206120706f69736f6e6f7573206d757368726f6f6d".to_string()),
            "SSdtIGtpbGxpbmcgeW91ciBicmFpbiBsaWtlIGEgcG9pc29ub3VzIG11c2hyb29t".to_string());
    }

    #[test]
    fn ch2() {
        let bytes1 = decode("1c0111001f010100061a024b53535009181c".to_string()).unwrap();
        let bytes2 = decode("686974207468652062756c6c277320657965".to_string()).unwrap();
        assert_eq!(encode(fixed_xor::fixed_xor(
            bytes1, &bytes2)), "746865206b696420646f6e277420706c6179".to_string());
    }

    #[test]
    fn ch3() {
        let (res, _sum, _key) = single_byte_xor::decode_single_byte_xor(decode("1b37373331363f78151b7f2b783431333d78397828372d363c78373e783a393b3736").unwrap());
        assert_eq!(res, "Cooking MC's like a pound of bacon".to_string());
    }

    #[test]
    fn ch4() {
        let mut list = vec![];
        if let Ok(lines) = read_lines("tests/4.txt".to_string()) {
            for line in lines {
                if let Ok(hex_string) = line {
                    list.push(hex_string);
                }
            }
        }
        assert_eq!(detect_single_char_xor::detect_single_char_xor_from_list(list),
            "Now that the party is jumping\n".to_string());
    }

    #[test]
    fn ch5() {
        assert_eq!(repeating_key_xor::repeating_key_xor(
            "Burning 'em, if you ain't quick and nimble\nI go crazy when I hear a cymbal".to_string(), "ICE".to_string()), 
            "0b3637272a2b2e63622c2e69692a23693a2a3c6324202d623d63343c2a26226324272765272a282b2f20430a652e2c652a3124333a653e2b2027630c692b20283165286326302e27282f".to_string());
    }

    #[test]
    fn ch6() {
        assert_eq!(break_repeating_key_xor::hamming_distance("this is a test".as_bytes().to_vec(), "wokka wokka!!!".as_bytes().to_vec()), 37);
        let mut b64encoded = "".to_string();
        if let Ok(lines) = read_lines("tests/6.txt".to_string()) {
            for line in lines {
                if let Ok(base64) = line {
                    b64encoded += &base64;
                }
            }
        }
        let decoded = base64::decode(b64encoded).unwrap();
        assert_eq!(String::from_utf8(break_repeating_key_xor::break_repeating_key_xor(decoded)).unwrap(), "Terminator X: Bring the noise");
    }

    #[test]
    fn ch7() {
        let mut aes128encoded = "".to_string();
        if let Ok(lines) = read_lines("tests/7.txt".to_string()) {
            for line in lines {
                if let Ok(m) = line {
                    aes128encoded += &m;
                }
            }
        }
        let decoded = base64::decode(aes128encoded).unwrap();
        assert_eq!(String::from_utf8(aes_ecb::aes_ecb_decrypt(decoded, &"YELLOW SUBMARINE".as_bytes().to_vec())).unwrap(),
            "I\'m back and I\'m ringin\' the bell \nA rockin\' on the mike while the fly girls yell \nIn ecstasy in the back of me \nWell that\'s my DJ Deshay cuttin\' all them Z\'s \nHittin\' hard and the girlies goin\' crazy \nVanilla\'s on the mike, man I\'m not lazy. \n\nI\'m lettin\' my drug kick in \nIt controls my mouth and I begin \nTo just let it flow, let my concepts go \nMy posse\'s to the side yellin\', Go Vanilla Go! \n\nSmooth \'cause that\'s the way I will be \nAnd if you don\'t give a damn, then \nWhy you starin\' at me \nSo get off \'cause I control the stage \nThere\'s no dissin\' allowed \nI\'m in my own phase \nThe girlies sa y they love me and that is ok \nAnd I can dance better than any kid n\' play \n\nStage 2 -- Yea the one ya\' wanna listen to \nIt\'s off my head so let the beat play through \nSo I can funk it up and make it sound good \n1-2-3 Yo -- Knock on some wood \nFor good luck, I like my rhymes atrocious \nSupercalafragilisticexpialidocious \nI\'m an effect and that you can bet \nI can take a fly girl and make her wet. \n\nI\'m like Samson -- Samson to Delilah \nThere\'s no denyin\', You can try to hang \nBut you\'ll keep tryin\' to get my style \nOver and over, practice makes perfect \nBut not if you\'re a loafer. \n\nYou\'ll get nowhere, no place, no time, no girls \nSoon -- Oh my God, homebody, you probably eat \nSpaghetti with a spoon! Come on and say it! \n\nVIP. Vanilla Ice yep, yep, I\'m comin\' hard like a rhino \nIntoxicating so you stagger like a wino \nSo punks stop trying and girl stop cryin\' \nVanilla Ice is sellin\' and you people are buyin\' \n\'Cause why the freaks are jockin\' like Crazy Glue \nMovin\' and groovin\' trying to sing along \nAll through the ghetto groovin\' this here song \nNow you\'re amazed by the VIP posse. \n\nSteppin\' so hard like a German Nazi \nStartled by the bases hittin\' ground \nThere\'s no trippin\' on mine, I\'m just gettin\' down \nSparkamatic, I\'m hangin\' tight like a fanatic \nYou trapped me once and I thought that \nYou might have it \nSo step down and lend me your ear \n\'89 in my time! You, \'90 is my year. \n\nYou\'re weakenin\' fast, YO! and I can tell it \nYour body\'s gettin\' hot, so, so I can smell it \nSo don\'t be mad and don\'t be sad \n\'Cause the lyrics belong to ICE, You can call me Dad \nYou\'re pitchin\' a fit, so step back and endure \nLet the witch doctor, Ice, do the dance to cure \nSo come up close and don\'t be square \nYou wanna battle me -- Anytime, anywhere \n\nYou thought that I was weak, Boy, you\'re dead wrong \nSo come on, everybody and sing this song \n\nSay -- Play that funky music Say, go white boy, go white boy go \nplay that funky music Go white boy, go white boy, go \nLay down and boogie and play that funky music till you die. \n\nPlay that funky music Come on, Come on, let me hear \nPlay that funky music white boy you say it, say it \nPlay that funky music A little louder now \nPlay that funky music, white boy Come on, Come on, Come on \nPlay that funky music \n\u{4}\u{4}\u{4}\u{4}".to_string());
    }

    #[test]
    fn ch8() {
        if let Ok(lines) = read_lines("tests/8.txt".to_string()) {
            for line in lines {
                if let Ok(hexed) = line {
                    let data = decode(hexed).unwrap().to_vec();
                    if detect_aes_ecb::detect_aes_ecb(&data) {
                        assert_eq!(data, vec![216, 128, 97, 151, 64, 168, 161, 155, 120, 64, 168, 163, 28, 129, 10, 61, 8, 100, 154, 247, 13, 192, 111, 79, 213, 210, 214, 156, 116, 76, 210, 131, 226, 221, 5, 47, 107, 100, 29, 191, 157, 17, 176, 52, 133, 66, 187, 87, 8, 100, 154, 247, 13, 192, 111, 79, 213, 210, 214, 156, 116, 76, 210, 131, 148, 117, 201, 223, 219, 193, 212, 101, 151, 148, 157, 156, 126, 130, 191, 90, 8, 100, 154, 247, 13, 192, 111, 79, 213, 210, 214, 156, 116, 76, 210, 131, 151, 169, 62, 171, 141, 106, 236, 213, 102, 72, 145, 84, 120, 154, 107, 3, 8, 100, 154, 247, 13, 192, 111, 79, 213, 210, 214, 156, 116, 76, 210, 131, 212, 3, 24, 12, 152, 200, 246, 219, 31, 42, 63, 156, 64, 64, 222, 176, 171, 81, 178, 153, 51, 242, 193, 35, 197, 131, 134, 176, 111, 186, 24, 106]);
                    }
                }
            }
        }
    }
    
    #[test]
    fn ch9() {
        let incomplete_block = "YELLOW SUBMARINE".to_string();
        let complete_block = pkcs7_padding::pkcs7_padding(incomplete_block.as_bytes().to_vec(), 20);
        assert_eq!(String::from_utf8(complete_block).unwrap(), "YELLOW SUBMARINE\u{4}\u{4}\u{4}\u{4}".to_string());
    }

    #[test]
    fn ch10() {
        let message = "YELLOW SUBMARINE".to_string();
        let enc = aes_ecb::aes_ecb_encrypt(message.as_bytes().to_vec(), &"HOLLOW SUBMARINE".as_bytes().to_vec());
        let dec = aes_ecb::aes_ecb_decrypt(enc, &"HOLLOW SUBMARINE".as_bytes().to_vec());
        assert_eq!(message.as_bytes().to_vec(), dec);

        let mut b64encoded = "".to_string();
        if let Ok(lines) = read_lines("tests/10.txt".to_string()) {
            for line in lines {
                if let Ok(base64) = line {
                    b64encoded += &base64;
                }
            }
        }
        let decoded = base64::decode(b64encoded).unwrap();
        println!("{}", decoded.len());
        assert_eq!(String::from_utf8(aes_cbc::aes_cbc_decrypt(decoded, &"YELLOW SUBMARINE".as_bytes().to_vec(), &vec![0; 16])).unwrap(),
                   "I\'m back and I\'m ringin\' the bell \nA rockin\' on the mike while the fly girls yell \nIn ecstasy in the back of me \nWell that\'s my DJ Deshay cuttin\' all them Z\'s \nHittin\' hard and the girlies goin\' crazy \nVanilla\'s on the mike, man I\'m not lazy. \n\nI\'m lettin\' my drug kick in \nIt controls my mouth and I begin \nTo just let it flow, let my concepts go \nMy posse\'s to the side yellin\', Go Vanilla Go! \n\nSmooth \'cause that\'s the way I will be \nAnd if you don\'t give a damn, then \nWhy you starin\' at me \nSo get off \'cause I control the stage \nThere\'s no dissin\' allowed \nI\'m in my own phase \nThe girlies sa y they love me and that is ok \nAnd I can dance better than any kid n\' play \n\nStage 2 -- Yea the one ya\' wanna listen to \nIt\'s off my head so let the beat play through \nSo I can funk it up and make it sound good \n1-2-3 Yo -- Knock on some wood \nFor good luck, I like my rhymes atrocious \nSupercalafragilisticexpialidocious \nI\'m an effect and that you can bet \nI can take a fly girl and make her wet. \n\nI\'m like Samson -- Samson to Delilah \nThere\'s no denyin\', You can try to hang \nBut you\'ll keep tryin\' to get my style \nOver and over, practice makes perfect \nBut not if you\'re a loafer. \n\nYou\'ll get nowhere, no place, no time, no girls \nSoon -- Oh my God, homebody, you probably eat \nSpaghetti with a spoon! Come on and say it! \n\nVIP. Vanilla Ice yep, yep, I\'m comin\' hard like a rhino \nIntoxicating so you stagger like a wino \nSo punks stop trying and girl stop cryin\' \nVanilla Ice is sellin\' and you people are buyin\' \n\'Cause why the freaks are jockin\' like Crazy Glue \nMovin\' and groovin\' trying to sing along \nAll through the ghetto groovin\' this here song \nNow you\'re amazed by the VIP posse. \n\nSteppin\' so hard like a German Nazi \nStartled by the bases hittin\' ground \nThere\'s no trippin\' on mine, I\'m just gettin\' down \nSparkamatic, I\'m hangin\' tight like a fanatic \nYou trapped me once and I thought that \nYou might have it \nSo step down and lend me your ear \n\'89 in my time! You, \'90 is my year. \n\nYou\'re weakenin\' fast, YO! and I can tell it \nYour body\'s gettin\' hot, so, so I can smell it \nSo don\'t be mad and don\'t be sad \n\'Cause the lyrics belong to ICE, You can call me Dad \nYou\'re pitchin\' a fit, so step back and endure \nLet the witch doctor, Ice, do the dance to cure \nSo come up close and don\'t be square \nYou wanna battle me -- Anytime, anywhere \n\nYou thought that I was weak, Boy, you\'re dead wrong \nSo come on, everybody and sing this song \n\nSay -- Play that funky music Say, go white boy, go white boy go \nplay that funky music Go white boy, go white boy, go \nLay down and boogie and play that funky music till you die. \n\nPlay that funky music Come on, Come on, let me hear \nPlay that funky music white boy you say it, say it \nPlay that funky music A little louder now \nPlay that funky music, white boy Come on, Come on, Come on \nPlay that funky music \n\u{4}\u{4}\u{4}\u{4}".to_string());
    }

    #[test]
    fn ch11() {
        let message = "YELLOW SUBMARINE".to_string();
        let enc = aes_cbc::aes_cbc_encrypt(message.as_bytes().to_vec(), &"HOLLOW SUBMARINE".as_bytes().to_vec(), &vec![0; 16]);
        let dec = aes_cbc::aes_cbc_decrypt(enc, &"HOLLOW SUBMARINE".as_bytes().to_vec(), &vec![0; 16]);
        assert_eq!(message.as_bytes().to_vec(), dec);

        // assert_eq!(detect_aes_mode::detect_aes_mode(
        //     detect_aes_mode::encryption_oracle(
        //         "YELLOW SUBMARINEYELLOW SUBMARINEYELLOW SUBMARINEYELLOW SUBMARINEYELLOW SUBMARINEYELLOW SUBMARINE".as_bytes().to_vec())), "CBC".to_string());
    }

    #[test]
    fn ch12() {
        let base64_added_data = "Um9sbGluJyBpbiBteSA1LjAKV2l0aCBteSByYWctdG9wIGRvd24gc28gbXkgaGFpciBjYW4gYmxvdwpUaGUgZ2lybGllcyBvbiBzdGFuZGJ5IHdhdmluZyBqdXN0IHRvIHNheSBoaQpEaWQgeW91IHN0b3A/IE5vLCBJIGp1c3QgZHJvdmUgYnkK".as_bytes();
        let decoded_added_data = base64::decode(base64_added_data).unwrap();
        let key = (0..16).collect();

        assert_eq!(one_byte_ecb_decryption::one_byte_ecb_decryption(
            &key)[..decoded_added_data.len()].to_vec(), decoded_added_data);
    }

    #[test]
    fn ch13() {
        let mut rng = rand::thread_rng();

        let mut key = vec![0; 16];
        for index in 0..key.len() {
            key[index] = rng.gen::<u8>();
        }
        let cookie_with_admin = ecb_cut_paste::create_cookie_with_admin_role(&key);
        let profile_with_admin = ecb_cut_paste::profile_decrypt(cookie_with_admin, &key);
        assert_eq!(profile_with_admin.role, "admin");
    }

    #[test]
    fn ch14() {
        let mut rng = rand::thread_rng();

        let rand_length = rng.gen_range(1..100);
        let mut rand_prefix = vec![0; rand_length];
        for el in rand_prefix.iter_mut() {
            *el = rng.gen::<u8>();
        }

        let mut key = vec![0; 16];
        for el in key.iter_mut() {
            *el = rng.gen::<u8>();
        }

        let base64_added_data = "Um9sbGluJyBpbiBteSA1LjAKV2l0aCBteSByYWctdG9wIGRvd24gc28gbXkgaGFpciBjYW4gYmxvdwpUaGUgZ2lybGllcyBvbiBzdGFuZGJ5IHdhdmluZyBqdXN0IHRvIHNheSBoaQpEaWQgeW91IHN0b3A/IE5vLCBJIGp1c3QgZHJvdmUgYnkK".as_bytes();
        let decoded_added_data = base64::decode(base64_added_data).unwrap();

        assert_eq!(one_byte_ecb_decryption_hard::one_byte_ecb_decryption_with_prefix(
            &key, &rand_prefix), decoded_added_data);
    }

    #[test]
    fn ch16() {
        let mut rng = rand::thread_rng();
        let mut key = vec![0; 16];
        for el in key.iter_mut() {
            *el = rng.gen::<u8>();
        }
        let mut iv = vec![0; 16];
        for el in iv.iter_mut() {
            *el = rng.gen::<u8>();
        }
        // userdata is supposed to be the same length as ";admin=true"
        let userdata = "hahahahahah".as_bytes().to_vec();
        let mut cookie = cbc_bit_flipping::oracle(&userdata, &key, &iv);
        cbc_bit_flipping::cookie_with_admin_role_attack(&mut cookie, &userdata, 16);
        
        assert_eq!(cbc_bit_flipping::oracle_is_admin(&cookie, &key, &iv), true);
    }
}

fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
where P: AsRef<Path>, {
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file).lines())
}
